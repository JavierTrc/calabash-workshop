# Description

Calabash sample using a compiled apk given as is

# Run

It's neccesary to have a connected device (or emulator) and the android SDK installed. Run with ruby bundler to guarantee that the correct version of the dependencies are used.

bundler exec calabash-android run "Transmilenio y Sitp_v20.3.1_apkpure.com.apk"